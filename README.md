# MZE blog (Helm Chart)

This Helm chart facilitates deployment of my personal blog.

## Quickstart
    user@host: helm repo add jekyll-mze https://gitlab.com/mzezulka/website/mze-helm
    user@host: helm install jekyll-mze jekyll-mze/jekyll-mze
    
##

You need to specify `docker.image.version` property which is not set by default on purpose.

E.g.:

`helm template . --set docker.image.version=1.0.0`
